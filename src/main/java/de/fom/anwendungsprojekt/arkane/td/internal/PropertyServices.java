package de.fom.anwendungsprojekt.arkane.td.internal;

import com.almasb.fxgl.core.EngineService;
import com.almasb.fxgl.core.collection.PropertyMap;

public class PropertyServices extends EngineService {

	private final String MONEY = "money";
	private final String POINTS = "points";
	private PropertyMap properties = new PropertyMap();
	
	public PropertyServices() {
		properties.setValue(MONEY, Integer.valueOf(0));
		properties.setValue(POINTS, Integer.valueOf(0));
	}
	
	public Integer getMoney() {
		return properties.getInt(MONEY);
	}
	
	public void setMoney(Integer value) {
		properties.setValue(MONEY, value);
	};
	
	/**
	 * Adds the given value to the current value of the currency
	 * 
	 * @param value - the value adding to the existing amount
	 */
	public void addMoney(Integer value) {
		Integer newValue = value + properties.getInt(MONEY);
		properties.setValue(MONEY, newValue);
	}

	public Integer getPoints() {
		return properties.getInt(POINTS);
	}
	
	public void setPOINTS(Integer value) {
		properties.setValue(POINTS, value);
	};
	
	/**
	 * Adds the given value to the current value of the points
	 * 
	 * @param value - the value adding to the existing amount
	 */
	public void addPoints(Integer value) {
		Integer newValue = value + properties.getInt(POINTS);
		properties.setValue(POINTS, newValue);
	}
}
