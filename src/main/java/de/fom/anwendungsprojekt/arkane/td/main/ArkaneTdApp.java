package de.fom.anwendungsprojekt.arkane.td.main;

import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.localization.Language;

import de.fom.anwendungsprojekt.arkane.td.compenent.EnemyComponent;
import de.fom.anwendungsprojekt.arkane.td.compenent.EnemyStats;
import de.fom.anwendungsprojekt.arkane.td.compenent.LevelData;
import de.fom.anwendungsprojekt.arkane.td.compenent.Path;
import de.fom.anwendungsprojekt.arkane.td.compenent.TowerStats;
import de.fom.anwendungsprojekt.arkane.td.compenent.WaveInformation;
import de.fom.anwendungsprojekt.arkane.td.compenent.WayComponent;
import de.fom.anwendungsprojekt.arkane.td.internal.DataLoaderServices;
import de.fom.anwendungsprojekt.arkane.td.internal.PropertyServices;

import static com.almasb.fxgl.dsl.FXGL.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class ArkaneTdApp extends GameApplication {

	private List<TowerStats> towerStatList;
	private List<EnemyStats> enemyStatList;
	private List<WaveInformation> waveInfomations;
	private List<LevelData> levelData;
	private Path path;
	private Integer towerChoise = 0;
	private Button nextWaveButton;
	
	
	private final String LIFE = "life";
	private final String NUMBER_OF_ENEMIES = "numberOfEnemies";
	private final String POINTS = "points";
	private final String MONEY = "money";
	private final String CURRENT_WAVE = "currentWave";

	@Override
	protected void initSettings(GameSettings settings) {
		settings.setWidth(1024);
		settings.setHeight(932);
		settings.setTitle("Arkane TD");
		settings.setVersion("0.0.0-SNAPSHOT");
		settings.setMainMenuEnabled(true);
		settings.setDefaultLanguage(Language.GERMAN);
		settings.addEngineService(PropertyServices.class);
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	protected void initGameVars(Map<String, Object> vars) {
		vars.put(LIFE, 20);
		vars.put(NUMBER_OF_ENEMIES, 0);
		vars.put(POINTS, 0);
		vars.put(MONEY, 175);
		vars.put(CURRENT_WAVE, 0);
	}

	@Override
	public void initGame() {
		towerStatList = new ArrayList<>(DataLoaderServices.loadListOfTowerStats());
		enemyStatList = new ArrayList<>(DataLoaderServices.loadListOfEnemyStats());
		waveInfomations = new ArrayList<>(DataLoaderServices.loadListOfWaveInfomation());
		levelData = new ArrayList<>(DataLoaderServices.loadListOfLevelData());
		getGameWorld().addEntityFactory(new ArkaneTdFactory());

		// watch the number of enemies and the current live
		getWorldProperties().<Integer>addListener(NUMBER_OF_ENEMIES, (old, newValue) -> {
			if (newValue <= 0) {
				endOfWave();
			}
		});

		getWorldProperties().<Integer>addListener(LIFE, (old, newValue) -> {
			if (newValue <= 0) {
				endGame(true);
			}
		});
		
        setLevelFromMap("tmx/" + levelData.get(0).getMap());
        
        List<Point2D> wayPoints = getGameWorld().getEntitiesByType(EntityType.WAY_POINT)
        										.stream()
        										.map(entity -> entity.getComponent(WayComponent.class))
        										.sorted(Comparator.comparingInt(WayComponent::getId))
        										.map(wayComponent -> new Point2D(wayComponent.getX(), wayComponent.getY()))
        										.collect(Collectors.toList());
        path = new Path(wayPoints);
	}

	/**
	 * 
	 */
	private void endOfWave() {
		// check if it is the last wave
		if (geti(CURRENT_WAVE) >= waveInfomations.size()) {
			endGame(false);
		} else {
			nextWaveButton.setDisable(false);
		}
	}

	/**
	 * 
	 * @param gameOver
	 */
	private void endGame(boolean gameOver) {
		String endMessage;

		if (gameOver) {
			endMessage = "Du hast verloren! \n"
					+ "Dein Highscore beträgt " + geti(POINTS) + " Punkte";
		} else {
			endMessage = "Du hast gewonnen! \n"
					+ "Dein Highscore beträgt " + geti(POINTS) + " Punkte";
		}
		showMessage(endMessage, getGameController()::gotoMainMenu);
	}

	@Override
	protected void initUI() {

		VBox hud = new VBox(10);

		Text currentLifeHeader = new Text("Leben:");
		currentLifeHeader.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
		Text currentLife = new Text();
		currentLife.textProperty().bind(getWorldProperties().intProperty(LIFE).asString());

		Text currentMoneyHeader = new Text("Arkanum:");
		currentMoneyHeader.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
		Text currentMoneyText = new Text();
		currentMoneyText.textProperty().bind(getWorldProperties().intProperty(MONEY).asString());

		HBox lifeBox = new HBox(10, currentLifeHeader, currentLife);
		HBox moneyBox = new HBox(10, currentMoneyHeader, currentMoneyText);
		HBox buttonBox = new HBox(10);
		HBox nextBox = new HBox(10);
		// Button-Menü
		List<Button>towerButtons = new ArrayList<>();
		for (int i = 0; i < towerStatList.size(); i++) {
			Button button = new Button(towerStatList.get(i).getName());
			button.setPrefSize(75, 75);
			final int ifin = i;
			button.setOnAction(event -> {
				towerChoise = ifin;
			});
			towerButtons.add(button);
		}

		nextWaveButton = new Button("Next Round");
		nextWaveButton.setPrefSize(125, 75);
		nextWaveButton.setOnAction(event -> {
			startNextWave();
			nextWaveButton.setDisable(true);
		});

		hud.getChildren().addAll(lifeBox, moneyBox);
		hud.setTranslateX(getAppWidth() - 900);
		hud.setTranslateY(getAppHeight() - 75);

		buttonBox.getChildren().addAll(towerButtons);
		buttonBox.setTranslateX(getAppWidth() - 600);
		buttonBox.setTranslateY(getAppHeight() - 90);
		
		nextBox.getChildren().addAll(nextWaveButton);
		nextBox.setTranslateX(getAppWidth() - 200);
		nextBox.setTranslateY(getAppHeight() - 90);		

		getGameScene().addUINode(hud);
		getGameScene().addUINode(buttonBox);
		getGameScene().addUINode(nextBox);

	}

	private void startNextWave() {	
		// Wenn die Welle läuft, macht der Button nix 
		if(geti(NUMBER_OF_ENEMIES) > 0) {
			return;
		}
		int waveNumber = geti(CURRENT_WAVE);
		inc(CURRENT_WAVE, 1);
		spawnWave(waveInfomations.get(waveNumber), path);
	}

	/**
	 * 
	 * @param cell
	 */
	public void onCellClicked(Entity cell) {
		TowerStats tower = towerStatList.get(towerChoise);
		if (cell.getProperties().exists("tower")) {
			return;
		}
		Integer towerCosts = tower.getCost();
		if (geti(MONEY) < towerCosts) {
			showMessage("Du hast zu wenig Geld für " + tower.getName() + "\nEs wird " + tower.getCost() + " benötigt!");
			return;
		}
		inc(MONEY, towerCosts * -1);
		
		spawn("tower",
				new SpawnData(cell.getPosition()).put("towerStats",tower));

	}

	/**
	 * Enemy finished the waypoints and the life and numbers of enemies will be decreased.
	 * The entity will also be removed fom the world 
	 * @param enemy
	 */
	public void enemyFinished(EnemyComponent enemy) {
		inc(LIFE, enemy.getEnemyStats().getKillingReward() * -1);
		inc(NUMBER_OF_ENEMIES, -1);
		enemy.getEntity().removeFromWorld();
	}

	public void enemyKilled(EnemyComponent enemy) {
		inc(POINTS, enemy.getEnemyStats().getKillingReward());
		inc(MONEY, enemy.getEnemyStats().getKillingReward());
		inc(NUMBER_OF_ENEMIES, -1);
	}
	

    private void spawnWave(WaveInformation wave, Path path) {
		inc(NUMBER_OF_ENEMIES, wave.getNumberOfEnemies());
		
		EnemyStats enemyStatsTemp = new EnemyStats();
		for (EnemyStats enemy : enemyStatList) {
			if (enemy.getName().equals(wave.getEnemy())) {
				enemyStatsTemp = enemy;
			}
		}
		
		EnemyStats enemyStats = enemyStatsTemp;
		
        runOnce(() -> {
            run(() -> {
            	spawn("enemy", new SpawnData(path.getStartingPoint())
    					.put("enemyStats", enemyStats)
    					.put("wayPoints", path.getWayPoints()));
            }, Duration.seconds(wave.getInterval()), wave.getNumberOfEnemies());
        }, Duration.seconds(3));
    }
}
