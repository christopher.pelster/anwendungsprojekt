package de.fom.anwendungsprojekt.arkane.td.compenent;

public class EnemyStats {

	private Double maxHealth;
	private Double movementSpeed;
	private Integer killingReward;
	private String iconLink;
	private String name;
	
	public EnemyStats() {
		this.name = "";
		this.maxHealth = Double.valueOf(0);
		this.movementSpeed = Double.valueOf(0);
		this.killingReward = Integer.valueOf(0);
		this.iconLink = "";
	}

	public Double getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(Double maxHealth) {
		this.maxHealth = maxHealth;
	}

	public Double getMovementSpeed() {
		return movementSpeed;
	}

	public void setMovementSpeed(Double movementSpeed) {
		this.movementSpeed = movementSpeed;
	}

	public Integer getKillingReward() {
		return killingReward;
	}

	public void setKillingReward(Integer killingReward) {
		this.killingReward = killingReward;
	}

	public String getIconLink() {
		return iconLink;
	}

	public void setIconLink(String iconLink) {
		this.iconLink = iconLink;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
