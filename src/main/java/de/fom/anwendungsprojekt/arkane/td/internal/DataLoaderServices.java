package de.fom.anwendungsprojekt.arkane.td.internal;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.almasb.fxgl.dsl.FXGL;

import de.fom.anwendungsprojekt.arkane.td.compenent.EnemyStats;
import de.fom.anwendungsprojekt.arkane.td.compenent.LevelData;
import de.fom.anwendungsprojekt.arkane.td.compenent.TowerStats;
import de.fom.anwendungsprojekt.arkane.td.compenent.WaveInformation;

public class DataLoaderServices {

	/**
	 * Get a list of all {@code TowerStats} in the resources with the {@code AssetLoaderService} 
	 *  
	 * @return List of all {@code TowerStats}.
	 */
	public static List<TowerStats> loadListOfTowerStats() {
		
		return Stream.of("canon", 
						 "rocketLauncher", 
					     "sniper")
					 .map(name -> FXGL.getAssetLoader().loadJSON("json/tower/" + name + ".json", TowerStats.class).get())
				     .collect(Collectors.toList());
	}
	
	/**
	 * Get a list of all {@code EnemieStats} in the resources with the {@code AssetLoaderService} 
	 *  
	 * @return List of all {@code EnemieStats}.
	 */
	public static List<EnemyStats> loadListOfEnemyStats() {
		
		return Stream.of("enemie1", 
						 "enemie2", 
 				 		 "enemie3")
					 .map(name -> FXGL.getAssetLoader().loadJSON("json/enemie/" + name + ".json", EnemyStats.class).get())
				     .collect(Collectors.toList());
	}
	
	/**
	 * Get a list of all {@code WaveInformation} in the resources with the {@code AssetLoaderService} 
	 *  
	 * @return List of all {@code WaveInformation}.
	 */
	public static List<WaveInformation> loadListOfWaveInfomation() {
		
		return Stream.of("wave1"
					   , "wave2"
					   , "wave3"
					   , "wave4"
						 )
					 .map(name -> FXGL.getAssetLoader().loadJSON("json/waveInformation/" + name + ".json", WaveInformation.class).get())
				     .collect(Collectors.toList());
	}
	
//	/**
//	 * Get a list of all {@code EnemieStats} in the resources with the {@code AssetLoaderService} 
//	 *  
//	 * @return List of all {@code EnemieStats}.
//	 */
	public static List<LevelData> loadListOfLevelData() {
		
		return Stream.of("level1")
					 .map(name -> FXGL.getAssetLoader().loadJSON("json/level/" + name + ".json", LevelData.class).get())
				     .collect(Collectors.toList());
	}
}
