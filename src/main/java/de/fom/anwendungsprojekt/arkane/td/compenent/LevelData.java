package de.fom.anwendungsprojekt.arkane.td.compenent;

public class LevelData {
	private String name;
	private String map;
	
	public LevelData() {
		this.name = "";
		this.map = "";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
}