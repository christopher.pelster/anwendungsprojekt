package de.fom.anwendungsprojekt.arkane.td.main;

/**
 * All the Entities in the tower defense game
 */
public enum EntityType {
	TOWER, ENEMY, PROJECTILE, TOWER_PLACE, WAY_POINT
}
