package de.fom.anwendungsprojekt.arkane.td.compenent;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Point2D;

public class Path {

	private List<Point2D> wayPoints;
	
	public Path(List<Point2D> wayPoints) {
		this.wayPoints = new ArrayList<>(wayPoints);
	}

	public List<Point2D> getWayPoints() {
		return this.wayPoints;
	}
	
	public Point2D getStartingPoint() {
		return this.wayPoints.get(0);
	}
}
