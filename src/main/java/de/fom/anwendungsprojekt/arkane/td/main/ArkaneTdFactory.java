package de.fom.anwendungsprojekt.arkane.td.main;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.dsl.components.AutoRotationComponent;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.EntityFactory;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.Spawns;
import com.almasb.fxgl.entity.components.TimeComponent;

import de.fom.anwendungsprojekt.arkane.td.compenent.EnemyComponent;
import de.fom.anwendungsprojekt.arkane.td.compenent.EnemyStats;
import de.fom.anwendungsprojekt.arkane.td.compenent.ProjectileComponent;
import de.fom.anwendungsprojekt.arkane.td.compenent.TowerComponent;
import de.fom.anwendungsprojekt.arkane.td.compenent.TowerStats;
import de.fom.anwendungsprojekt.arkane.td.compenent.WayComponent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ArkaneTdFactory implements EntityFactory {
	 
	/**
     * Spawns the tower place on the level where a tower can be placed
     * @param data contains the coordinates where to spawn
     * @return the spawned {@code Entity}
	 */
	@Spawns("towerPlace")
	public Entity spawnTowerPlace(SpawnData data) {
		Rectangle base = new Rectangle(64, 64, Color.GREEN);
		
		return FXGL.entityBuilder(data)
				.type(EntityType.TOWER_PLACE)
				.viewWithBBox(base)
				.onClick(click -> {
                    FXGL.<ArkaneTdApp>getAppCast().onCellClicked(click);
				})
				.build();
	}
	
	/**
     * Spawns the tower on the level and shoot at the nearest enemy in range
     * @param data contains the coordinates where to spawn, the {@code TowerStats}
     * 		  to create the {@code TowerComponent}  
     * @return the spawned {@code Entity}
	 */
	@Spawns("tower")
	public Entity spawnTower(SpawnData data) {
		TowerStats towerStats = data.get("towerStats");
		return FXGL.entityBuilder(data)
				.type(EntityType.TOWER)
				.viewWithBBox(towerStats.getIconLink())
				.with(new TowerComponent(towerStats))
				.collidable()
				.rotate(0)
				.build();
	}
	
	/**
     * Spawns the enemy on the level and will walk along the given way points
     * @param data contains the coordinates where to spawn, the {@code EnemyStats} and {@code List<Point2D>}
     * 		  to create the {@code EnemyComponent}  
     * @return the spawned {@code Entity}
	 */
    @Spawns("enemy")
    public Entity spawnEnemy(SpawnData data) {
        EnemyStats enemyStats = data.get("enemyStats");

        return FXGL.entityBuilder(data)
                .type(EntityType.ENEMY)
                .viewWithBBox(enemyStats.getIconLink())
                .collidable()
                .with(new TimeComponent())
                .with(new EnemyComponent(enemyStats, data.get("wayPoints")))
                .with(new AutoRotationComponent())
                .build();
    }
    
    /**
     * Spawns the waypoint on the level, where the enemy will walk along
     * @param data contains the coordinates where to spawn and an the id
     * @return the spawned {@code Entity}
     */
    @Spawns("wayPoint")
    public Entity newWaypoint(SpawnData data) {
        return FXGL.entityBuilder(data)
                .type(EntityType.WAY_POINT)
                .with(new WayComponent(data.get("id"), data.getX(), data.getY()))
                .build();
    }

    /**
     * Spawns the projectile on the level shooting in the direction of the enemy
     * @param data contains the coordinates where to spawn, the {@code TowerComponent} shooting it
     *        and the {@code EnemyComponent} where its shot at to create the {@code ProjectileComponent}
     * @return the spawned {@code Entity}
     */
    @Spawns("projectile")
    public Entity spawnBullet(SpawnData data) {
    	TowerComponent tower = data.get("tower");
    	
        return FXGL.entityBuilder(data)
                .type(EntityType.PROJECTILE)
                .viewWithBBox(FXGL.texture(tower.getTowerStats().getProjectileImageLink()))
                .collidable()
                .with(new ProjectileComponent(tower, data.get("enemy")))
                .with(new AutoRotationComponent())
                .build();
    }
}
