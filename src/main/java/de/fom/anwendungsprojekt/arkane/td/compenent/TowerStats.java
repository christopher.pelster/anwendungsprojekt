package de.fom.anwendungsprojekt.arkane.td.compenent;

public class TowerStats {
	
	private String name;
	private Double attackDamage;
	private Double attackRate;
	private Double attackRange;
	private String iconLink;
	private Integer cost;
	private Double multiplier;
	private Integer level;
	private String projectileImageLink; 
	private Double projectileSpeed;

	public TowerStats() {
		this.name = "";
		this.attackDamage = Double.valueOf(5);
		this.attackRange = Double.valueOf(0);
		this.attackRate = Double.valueOf(0.2);
		this.iconLink = "";
		this.cost = Integer.valueOf(0);
		this.multiplier = Double.valueOf(0);
		this.level = Integer.valueOf(0);
		this.projectileImageLink = "";
		this.projectileSpeed = Double.valueOf(300);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAttackDamage() {
		return attackDamage;
	}

	public void setAttackDamage(Double attackDamage) {
		this.attackDamage = attackDamage;
	}

	public Double getAttackRate() {
		return attackRate;
	}

	public void setAttackRate(Double attackRate) {
		this.attackRate = attackRate;
	}

	public Double getAttackRange() {
		return attackRange;
	}

	public void setAttackRange(Double attackRange) {
		this.attackRange = attackRange;
	}

	public String getIconLink() {
		return iconLink;
	}

	public void setIconLink(String iconLink) {
		this.iconLink = iconLink;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(Double multiplier) {
		this.multiplier = multiplier;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public String getProjectileImageLink() {
		return projectileImageLink;
	}
	
	public void setProjectileImageLink(String projectileImageLink) {
		this.projectileImageLink = projectileImageLink;
	}

	public Double getProjectileSpeed() {
		return projectileSpeed;
	}

	public void setProjectileSpeed(Double projectileSpeed) {
		this.projectileSpeed = projectileSpeed;
	}
}
