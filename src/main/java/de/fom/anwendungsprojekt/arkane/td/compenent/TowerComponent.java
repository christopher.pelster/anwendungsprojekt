package de.fom.anwendungsprojekt.arkane.td.compenent;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.time.LocalTimer;

import de.fom.anwendungsprojekt.arkane.td.main.EntityType;
import javafx.geometry.Point2D;
import javafx.util.Duration;

public class TowerComponent extends Component {

	private TowerStats towerStats;
	private LocalTimer timer;

	public TowerComponent(TowerStats towerStats) {
		this.towerStats = towerStats;
	}

	@Override
	public void onAdded() {
		timer = FXGL.newLocalTimer();
		timer.capture();
	}

	@Override
	public void onUpdate(double tpf) {

		FXGL.getGameWorld()
			.getClosestEntity(entity, e -> e.isType(EntityType.ENEMY))
											.ifPresent(nearestEnemy -> {
			if (nearestEnemy.getCenter().distance(entity.getCenter()) <= towerStats.getAttackRange()) {
				entity.rotateToVector(nearestEnemy.getPosition().subtract(entity.getPosition()));
				entity.rotateBy(90);
				if (timer.elapsed(Duration.seconds(1.5))) {
					shootProjectile(nearestEnemy);
					timer.capture();
				}
			}
		});

	}

	/**
	 * Spawns an shooting projectile starting from the tower towards the enemy
	 * 
	 * @param enemy
	 */
	private void shootProjectile(Entity enemy) {
		// calculate the direction from the tower to the enemy
		Point2D towerPosition = getEntity().getCenter();
		Point2D direction = enemy.getCenter().subtract(towerPosition);

		FXGL.spawn("projectile",
				new SpawnData(towerPosition)
					.put("imageLink", this.towerStats.getProjectileImageLink())
					.put("tower", this)
					.put("enemy", enemy.getComponent(EnemyComponent.class)))
					.rotateToVector(direction);
	}

	public TowerStats getTowerStats() {
		return this.towerStats;
	}

}
