package de.fom.anwendungsprojekt.arkane.td.compenent;

public class WaveInformation {
	private Integer waveNumber;
	private String enemy;
	private Integer numberOfEnemies;
	
	public WaveInformation() {
		this.setWaveNumber(Integer.valueOf(0));
		this.setEnemy("");
		this.setNumberOfEnemies(Integer.valueOf(0));
	}

	public Integer getWaveNumber() {
		return waveNumber;
	}

	public void setWaveNumber(Integer waveNumber) {
		this.waveNumber = waveNumber;
	}

	public String getEnemy() {
		return enemy;
	}

	public void setEnemy(String enemy) {
		this.enemy = enemy;
	}

	public Integer getNumberOfEnemies() {
		return numberOfEnemies;
	}

	public void setNumberOfEnemies(Integer numberOfEnemies) {
		this.numberOfEnemies = numberOfEnemies;
	}

	public Double getInterval() {
		return 1.0;
	}
	
}
