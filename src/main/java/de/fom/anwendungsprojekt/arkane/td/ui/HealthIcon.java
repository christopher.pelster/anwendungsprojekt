package de.fom.anwendungsprojekt.arkane.td.ui;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.texture.Texture;

import javafx.scene.Parent;

public class HealthIcon extends Parent {

	public HealthIcon() {
		Texture healthIcon = FXGL.texture("health.png");
		getChildren().addAll(healthIcon);
	}
}
