package de.fom.anwendungsprojekt.arkane.td.compenent;

import java.util.ArrayList;
import java.util.List;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;

import de.fom.anwendungsprojekt.arkane.td.main.ArkaneTdApp;
import javafx.geometry.Point2D;

public class EnemyComponent extends Component {
	
	private EnemyStats enemyStats;
	private List<Point2D> wayPoints;
	private Point2D nextWayPoint;
	private Double currentHealth;

	public EnemyComponent(EnemyStats enemyStats, List<Point2D> wayPoints) {
		this.enemyStats = enemyStats;
		this.wayPoints = new ArrayList<>(wayPoints);
		this.currentHealth = enemyStats.getMaxHealth();
	}

    @Override
    public void onAdded() {
    	// placing the enemy at the first waypoint
    	nextWayPoint = wayPoints.remove(0);
        entity.setPosition(nextWayPoint);
    }
    
    @Override
    public void onUpdate(double tpf) {
    	// move to the next waypoint
    	double speed = tpf * 60 * enemyStats.getMovementSpeed();
    	Point2D currentPosition = entity.getPosition();
    	Point2D vector = nextWayPoint.subtract(currentPosition)
    			                     .normalize()
    			                     .multiply(speed);
    	
    	this.entity.translate(vector);
    	
    	// if position is close to the next waypoint it will get there
    	if (currentPosition.distance(nextWayPoint) < speed) {
    		
    		// remove the entity or get nextWayPoint 
    		if(wayPoints.isEmpty()) {
    			FXGL.<ArkaneTdApp>getAppCast().enemyFinished(this);
    		} else {
    	        entity.setPosition(nextWayPoint);
    	    	nextWayPoint = wayPoints.remove(0);
    		}
    	}
    }
    
    public EnemyStats getEnemyStats() {
    	return this.enemyStats;
    }
    

	/**
	 * deal damage to the enemy
	 * @param damage - damage done to the enemy
	 * @return the current health of the enemy
	 */
	public Double attackEnemy(Double damage) {
		currentHealth -= damage;
		if (currentHealth < 0) {
			currentHealth = Double.valueOf(0);
		}
		return currentHealth;
	}
}
