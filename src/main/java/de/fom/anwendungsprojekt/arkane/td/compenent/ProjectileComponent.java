package de.fom.anwendungsprojekt.arkane.td.compenent;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;

import de.fom.anwendungsprojekt.arkane.td.main.ArkaneTdApp;

public class ProjectileComponent extends Component {

	private TowerComponent tower;
	private EnemyComponent enemy;
	
	public ProjectileComponent(TowerComponent tower, EnemyComponent enemy) {
		this.tower = tower;
		this.enemy = enemy;
	}
	

    @Override
    public void onUpdate(double tpf) {
    	// if enemy is already killed remove the projectile
        if (enemy.getEntity() == null || !enemy.getEntity().isActive()) {
            entity.removeFromWorld();
            return;
        } 
        
        double projectileSpeed = tpf * tower.getTowerStats().getProjectileSpeed();
        
        if (entity.distanceBBox(enemy.getEntity()) < projectileSpeed) {

            // if distance is lower then the projectile speed remove the projectile and damage the enemy
        	entity.removeFromWorld();
        	
        	double newHealth = enemy.attackEnemy(tower.getTowerStats().getAttackDamage());
        	if (newHealth == 0) {
        		FXGL.<ArkaneTdApp>getAppCast().enemyKilled(enemy);
        		enemy.getEntity().removeFromWorld();
        	}
        } else {
        	// move the projectile towards the enemy
        	entity.translateTowards(enemy.getEntity().getCenter(), projectileSpeed);
        }
    }
}
